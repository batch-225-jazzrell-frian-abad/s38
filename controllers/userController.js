const User = require('../models/user');
const bcrypt = require('bcrypt');




module.exports.checkEmailExist = (reqBody) => {

	return User.find({email : reqBody.email}).then(result => {

		// the findOne method returns a record if a match is found

		if (result.length > 0) {

			return {
				message : 'User was found'
			}

		} else if (result.length > 1) {

			return {
				message: 'Duplicate User'
			}

		} else {

			return {
				message: 'User not found'
			}
		}

	})
}



module.exports.registerUser = (reqBody) => {


	let newUser = new User({

		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	}) 

	return newUser.save().then((user, error) => {

		if (error) {

			return false;

		} else {

			return user

		};
	});
};


// Activity

module.exports.getProfile = (userID, newContent) => {

	return User.findById(userID).then((result, err) => {

		if (err){
			console.log(err)
			return false;
		} 
		else {
			
			result.password = "  ";

			return result.save().then((updated, saveErr) => {
				if (saveErr) {
					console.log(saveErr);
					return false;
				} else{
					return updated;
				}
			})
		}
	})
}