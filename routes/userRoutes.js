const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController')

// Check Email 
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(result => res.send(result));
});

// Route for registration

router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(result => res.send(result));

});

// Activity

router.post('/details', (req, res) => {
	userController.getProfile(req.body).then(result => res.send(result));
});


module.exports = router;